<?php

namespace App\Config\DI;

use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;

class Definitions
{

    public static function get(): array
    {
        return [
            ResponseInterface::class => new Response()
        ];
    }
}
