<?php

namespace App\Config;

class View
{
    private string $file = '';
    private mixed $container = [];

    public function __construct()
    {
    }

    public function file($file): self
    {
        $this->file = $file;
        return $this;
    }

    public function content(string $name, mixed $content): self
    {
        $this->container[$name] = $content;
        return $this;
    }

    public function render(): void
    {
        if (file_exists("../View/" . $this->file)) {
            $container = $this->container;
            require_once "../View/".$this->file;
        } else {
            echo "View not found";
        }
    }
}