<?php

namespace App\Config\Router;

use App\Controller\HelloJson;
use App\Controller\HelloWorld;

class Routes
{


    /**
     * @return Route[]
     */
    static function get(): array
    {
        return [
            Route::create()->method(HTTPMethod::GET)
                ->route('/hello')
                ->handler(HelloWorld::class),
            Route::create()->method(HTTPMethod::GET)
                ->route('/api/hello/{name}')
                ->handler(HelloJson::class)
        ];
    }
}