<?php

namespace App\Config\Router;

class Route
{

    private string $method;
    private string $route;
    private mixed $handler;


    public function __construct()
    {
    }

    static function create()
    {
        return new Route();
    }

    public function method(string $method) {
        $this->method = $method;
        return $this;
    }

    public function route(string $route) {
        $this->route = $route;
        return $this;
    }

    public function handler(mixed $handler) {
        $this->handler = $handler;
        return $this;
    }
    

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @return mixed
     */
    public function getHandler(): mixed
    {
        return $this->handler;
    }

}
