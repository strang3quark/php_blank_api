<?php

namespace App\Controller;

use App\Config\View;
use Psr\Http\Message\ResponseInterface;

class HelloWorld
{

    private View $view;

    public function __construct(View $view)
    {
        $this->view = $view;

        $view->file('HelloView.php');
    }

    public function __invoke(): ?ResponseInterface
    {
        $this->view->content("nome", "Bruno");
        $this->view->render();

        return null;
    }

}