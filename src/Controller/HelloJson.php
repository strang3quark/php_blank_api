<?php

namespace App\Controller;

use Laminas\Diactoros\ServerRequest;
use Psr\Http\Message\ResponseInterface;
use Relay\Runner;

class HelloJson
{

    private ResponseInterface $response;


    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function __invoke(ServerRequest $request, Runner $runner): ResponseInterface
    {
        $name = $request->getAttribute("name");

        $response = $this->response->withHeader('Content-Type', 'application/json');
        $response->getBody()
            ->write(json_encode([
                "message" => "Hello $name!"
            ]));

        return $response;
    }
}